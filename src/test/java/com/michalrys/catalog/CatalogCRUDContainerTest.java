package com.michalrys.catalog;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

public class CatalogCRUDContainerTest {
    @Test
    public void shouldAddFile() {
        // given
        Path path = new Path("C:\\test\\file.txt");
        File file = new File(path);
        HashSet<DataToStore> storage = new HashSet<>();
        CatalogCRUDContainer catalogCRUDContainer = new CatalogCRUDContainer();
        storage.add(file);

        // when
        catalogCRUDContainer.add(file);

        // then
        Assert.assertTrue(storage.contains(file));
        //Assert.assertEquals(file, catalogCRUDContainer.find(file));
    }

    @Test
    public void shouldFindFile() {
        // given
        Path path = new Path("C:\\test\\file.txt");
        File file = new File(path);
        HashSet<DataToStore> storage = new HashSet<>();
        CatalogCRUDContainer catalogCRUDContainer = new CatalogCRUDContainer();
        storage.add(file);
        catalogCRUDContainer.add(file);

        // when
        catalogCRUDContainer.find(file);

        // then
        Assert.assertEquals(file, catalogCRUDContainer.find(file));
    }

    @Test
    public void shouldAddImage() {
        // given
        Path path = new Path("C:\\test\\file.jpg");
        Image image = new Image(path);
        HashSet<DataToStore> storage = new HashSet<>();
        CatalogCRUDContainer catalogCRUDContainer = new CatalogCRUDContainer();
        storage.add(image);

        // when
        catalogCRUDContainer.add(image);

        // then
        Assert.assertTrue(storage.contains(image));
        Assert.assertEquals(image, catalogCRUDContainer.find(image));
    }
}