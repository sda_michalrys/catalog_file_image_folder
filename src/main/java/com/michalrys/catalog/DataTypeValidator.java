package com.michalrys.catalog;

class DataTypeValidator {
    boolean inputDataIsCorrect(String userGivenDataType) {
        //TODO implement it later
        for (DataType allowableValue : DataType.values()) {
            if (allowableValue.toString().equals(userGivenDataType)) {
                return true;
            }
        }
        return false;
        // TODO what is better?
//        try {
//            DataType.valueOf(userGivenDataType);
//        } catch (IllegalArgumentException exc) {
//            return false;
//        }
    }
}
