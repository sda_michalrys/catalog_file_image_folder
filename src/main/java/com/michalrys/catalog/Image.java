package com.michalrys.catalog;

class Image implements DataToStore {
    private final Path path;
    private final DataType dataType;

    Image(Path path) {
        this.path = path;
        dataType = DataType.IMAGE;
    }

    @Override
    public DataType getType() {
        return dataType;
    }

    @Override
    public Path getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Image image = (Image) o;

        if (path != null ? !path.equals(image.path) : image.path != null) return false;
        return dataType == image.dataType;
    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (dataType != null ? dataType.hashCode() : 0);
        return result;
    }
}
