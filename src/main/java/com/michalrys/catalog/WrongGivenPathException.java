package com.michalrys.catalog;

class WrongGivenPathException extends RuntimeException {
    WrongGivenPathException(String userGivenPath) {
        super("Given path is wrong: " + userGivenPath);
    }
}
