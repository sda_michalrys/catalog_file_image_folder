package com.michalrys.catalog;

enum DataType {
    FILE,
    FOLDER,
    IMAGE;
}
