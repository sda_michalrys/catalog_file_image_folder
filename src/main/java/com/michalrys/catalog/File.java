package com.michalrys.catalog;

class File implements DataToStore {
    private final Path path;
    private final DataType dataType;

    File(Path path) {
        this.path = path;
        dataType = DataType.FILE;
    }

    @Override
    public DataType getType() {
        return dataType;
    }

    @Override
    public Path getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        File file = (File) o;

        if (path != null ? !path.equals(file.path) : file.path != null) return false;
        return dataType == file.dataType;
    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (dataType != null ? dataType.hashCode() : 0);
        return result;
    }
}
