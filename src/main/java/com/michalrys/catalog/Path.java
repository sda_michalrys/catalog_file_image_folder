package com.michalrys.catalog;

class Path {
    private final String path;

    Path(String path) {
        this.path = path;
    }

    Path getPathWithSeparatorSlash() {
        String pathSlash = path.replace("\\", "/");
        return new Path(pathSlash);
    }

    Path getPathWithSeparatorBackslash() {
        String pathSlash = path.replace("/", "\\");
        return new Path(pathSlash);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Path path1 = (Path) o;

        return path != null ? path.equals(path1.path) : path1.path == null;
    }

    @Override
    public int hashCode() {
        return path != null ? path.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Path=" + path;
    }
}
