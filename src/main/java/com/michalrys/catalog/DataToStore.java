package com.michalrys.catalog;

interface DataToStore {
    DataType getType();
    Path getPath();
}
