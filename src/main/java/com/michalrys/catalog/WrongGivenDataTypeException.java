package com.michalrys.catalog;

class WrongGivenDataTypeException extends RuntimeException {
    WrongGivenDataTypeException(String userGivenDataType) {
        super("Given data type is wrong: " + userGivenDataType);
    }
}
