package com.michalrys.catalog;

public class UserControler {
    private CatalogCRUDContainer catalogCRUDContainer;
    private DataTypeValidator dataTypeValidator;
    private PathValidator pathValidator;

    public UserControler() {
        catalogCRUDContainer = new CatalogCRUDContainer();
        dataTypeValidator = new DataTypeValidator();
        pathValidator =  new PathValidator();
    }

//    Delete, because CatalogCRUDContainer cannot be visible in Main->psvm
//    public UserControler(CatalogCRUDContainer catalogCRUDContainer) {
//        this.catalogCRUDContainer = catalogCRUDContainer;
//    }

    public void add(String userGivenDataType, String userGivenPath) {
        // conversion string to enum and path
        if (!dataTypeValidator.inputDataIsCorrect(userGivenDataType)) {
            throw new WrongGivenDataTypeException(userGivenDataType);
        }
        if (!pathValidator.inputDataIsCorrect(userGivenPath)) {
            throw new WrongGivenPathException(userGivenPath);
        }
        DataType dataType = DataType.valueOf(userGivenDataType);
        Path path = new Path(userGivenPath);

//        // version with if statement
//        if (dataType == DataType.FILE) {
//            catalogCRUDContainer.add(new File(path));
//        }
//        if (dataType == DataType.IMAGE) {
//            catalogCRUDContainer.add(new Image(path));
//        }
//        if (dataType == DataType.FOLDER) {
//            catalogCRUDContainer.add(new Folder(path));
//        }

        // version with switch statement
        switch (dataType) {
            case FILE:
                catalogCRUDContainer.add(new File(path));
                break;
            case IMAGE:
                catalogCRUDContainer.add(new Image(path));
                break;
            case FOLDER:
                catalogCRUDContainer.add(new Folder(path));
                break;
            default:
        }
    }
}
