package com.michalrys.catalog;

import java.util.HashSet;
import java.util.Set;

class CatalogCRUDContainer {
    private Set<DataToStore> storage;

    CatalogCRUDContainer() {
        // use when you want to create new storage
        storage = new HashSet<>();
    }

//    CatalogCRUDContainer(HashSet<DataToStore> storage) {
//        this.storage = storage;
//    }

    void add(File file) {
        storage.add(file);
    }

    void add(Image image) {
        storage.add(image);
    }

    void add(Folder folder) {
        storage.add(folder);
    }

    File find(File file) {
        if (storage.contains(file)) {
            for (DataToStore data : storage) {
                if (data.equals(file)) {
                    return file;
                }
            }
        }
        return null;
    }

    Image find(Image image) {
        if (storage.contains(image)) {
            for (DataToStore data : storage) {
                if (data.equals(image)) {
                    return image;
                }
            }
        }
        return null;
    }


}
