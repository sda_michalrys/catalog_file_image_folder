package com.michalrys.catalog;

class Folder implements DataToStore {
    private final Path path;
    private final DataType dataType;

    Folder(Path path) {
        this.path = path;
        dataType = DataType.FOLDER;
    }

    @Override
    public DataType getType() {
        return dataType;
    }

    @Override
    public Path getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Folder folder = (Folder) o;

        if (path != null ? !path.equals(folder.path) : folder.path != null) return false;
        return dataType == folder.dataType;
    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (dataType != null ? dataType.hashCode() : 0);
        return result;
    }
}
